`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CP43 
// Engineer: 
//  Phongpak Taothong
// Create Date: 04/20/2019 11:07:32 AM
// Design Name: 
// Module Name: z80_tester
// Project Name: FinalProject 
// Target Devices: Basys3
// Tool Versions: 
// Description: 
//  Top Level of Pong Game
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// 
// 
//////////////////////////////////////////////////////////////////////////////////


module z80_tester(
    input clk, //Comment Out for simulation
    input wire reset, //Comment Out for simulation
    input PS2Data,
    input PS2Clk,
    input wire [11:0] sw,
    input wire btnU,
    input wire btnL,
    output [15:0] led,
    output [6:0] seg,
    output [3:0] an,
    output dp,
    output wire hsync, vsync,
	output wire [11:0] rgb
);

    reg nWAIT = 1'b1;
    reg nINT = 1'b1;
    reg nNMI = 1'b1;
    reg nRESET = 1'b1;
    reg nBUSRQ = 1'b1;
    wire [1:0] o1;
    wire [1:0] o2;
    wire [7:0] bx;
    wire gs;
    
//    reg reset = 1'b0; // For Simulation
//    reg clk = 1'b0; //For Simulation
//    wire nM1,nMREQ,nIORQ,nRD,nWR,nRFSH,nHALT,nBUSACK; // For Simulation

    // ----------------- INTERNAL WIRES -----------------//
    wire [15:0] A;
    wire [7:0] D;
    
    wire RamWE;
    assign RamWE = nIORQ==1 && nRD==1 && nWR==0;
    assign led[0] = gs; 
//    assign led[7:0] = bx;
//    assign led[1:0] = o2;
//    assign led[3:2] = o1; 
//    assign led[15:4] = A[15:4];

// Create Clock to SLOW DOWN CPU  
//    wire targetClk;
//    wire [12:0] tclk;
//    assign tclk[0] = clk;
//    genvar c;
//    generate for(c=0;c<12;c=c+1)
//    begin
//        clockDiv fdiv(tclk[c+1],tclk[c]);
//    end endgenerate
//    clockDiv fdivTarget(targetClk,tclk[12]);
    
    z80_top_direct_n cpu(nM1,nMREQ,nIORQ,nRD,nWR, nRFSH,nHALT,nBUSACK,nWAIT,nINT,nNMI,nRESET,nBUSRQ,clk,A,D);

    mem_mapped_keyboard mem(D,A,PS2Data,PS2Clk,RamWE,clk,reset,sw,btnU,btnL,seg,an,dp,hsync,vsync,rgb,o1,o2,bx,gs);


//FOR SIMULATION
//    initial
//    begin
//        #0;
//        nWAIT = 1;
//        nINT = 1;
//        nNMI = 1;
//        nRESET = 1;
//        nBUSRQ = 1;
//        reset = 0;
//        #100;
//        nRESET = 1;
//        #150;
//        nRESET = 1;
//        #160;
//        nNMI = 1;
//        #200;
//        nNMI = 1;
//     end

//    always
//    begin
//        #2 clk = ~clk; 
//    end
    
//END FOR SIMULATION
endmodule
