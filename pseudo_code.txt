/****
panel axis: top 0 left 0    base on VGA render
register:
    ball_x          ball position at left
    ball_y          ball position at bottom
    left_bar_y      bar position at bottom
    right_bar_y     bar position at bottom
    stage           0 for start_stage | 1 for play_stage | [optional: 2 for score_stage | 3 for loading_stage/pause_stage]
    restart         restart flag; 1 for restart
    left_score      score for player left    
    right_score     score for player right    
    time            time remain , decrease to 0 [optinal: add penalty when ball go over edge]
    ball_speed_x    0 for moving left | 1 for moving right        
    ball_speed_y    0 for moving top | 1 for moving bottom
all(?) custom config:
    #main_line_id
    #startposition
    #totaltime
    #ballheight
    #ballwidth
    #barheight
    #rightplayerline
    #leftplayerline
    #rightedge
    #leftedge
    #1pixel
    #1point
    #1timetick
    #5timeticks
***/

//position reg: ball_x | ball_y | left_bar_y | right_bar_y
//other reg: stage | restart | left_score | right_score | time | ball_speed_x | ball_speed_y

//main
compare stage == 1
else goto #main_line_id
    if stage 0 && restart == 1
        load stage 1
        load ball_x #startposition
        load ball_y #startposition
        load ball_speed_x 1
        load ball_speed_y 0
        load left_bar_y #startposition
        load right_bar_y #startposition
        load time #totaltime
        load left_score 0
        load right_score 0
        load restart 0
        goto #main_line_id
    if stage 1
        if time > 0
            load stage 0 //or 2
        if ball_x == #leftplayerline && left_bar_y - #ballheight + #1pixel < ball_y < left_bar_y + #barheight + #ballheight - #1pixel
            load ball_speed_x 1
        if ball_x + #ballwidth == #rightplayerline && right_bar_y - #ballheight + #1pixel < ball_y < right_bar_y + #barheight + #ballheight - #1pixel
            load ball_speed_x 0
        if ball_x < #leftedge //+ #ballwidth 
            add right_score #1point
            // sub time #5timeticks
            load ball_x #startposition
            load ball_y #startposition
        if ball_x > #rightedge
            add left_score #1point
            // sub time #5timeticks
            load ball_x #startposition
            load ball_y #startposition
        if ball_y > #bottomedge
            load ball_speed_y 0
        if ball_y < #topedge //- #ballheight 
            load ball_speed_y 1
        if ball_speed_x == 1
            add ball_x #1pixel
        else sub ball_x #1pixel
        if ball_speed_y == 1
            add ball_y #1pixel
        else sub ball_y #1pixel
        sub time #1timetick
        goto #main_line_id 

//interupt
//press up_p1: 
    if stage == 1 && left_bar_y < top
    add left_bar_y #1pixel
    goto #main_line_id

//press down_p1: 
    if stage == 1 && left_bar_y > bottom
    sub left_bar_y #1pixel
    goto #main_line_id

//press up_p2: 
    if stage == 1 && left_bar_y < top
    add right_bar_y #1pixel
    goto #main_line_id

//press down_p2: 
    if stage == 1 && left_bar_y > bottom
    sub right_bar_y #1pixel
    goto #main_line_id

//press start:
    load restart 1
    goto #main_line_id
